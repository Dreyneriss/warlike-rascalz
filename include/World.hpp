

//include guard
#ifndef WORLD_HPP
#define WORLD_HPP


//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow
#include <iostream>
#include <fstream>
#include <cmath>


class World{
	private:
		
		// background
		sf::Texture tex_background;
		sf::Sprite spr_background;
		
		// ground
		sf::Image img_ground;
		sf::Texture tex_ground;
		sf::Sprite terrain;
		
		int width;
		int heigh;
		int nbPix;
		
		/*Des états des pixels...
		* Je pensais faire des pixels des unités de marching squares, mais y'a peut être plus simple.
		* Actuellement, un pixel plein peut avoir deux états, selon qu'il est voisin ou non d'un pixel vide.
		* Je peux rajouter des états selon leurs conditions physiques (avec indicateurs couleur).
		* Un pixel abimé empêcherait les objets de passer mais serait plus perméable aux balles et aux explosions.
		* Un pixel fragilisé ne serait un freint qu'à, éventuellement, la visibilité ou les fluides.
		* Ajouter une distinction fragilisé/fragile ? Genre, une vitre est fragile mais impérméable, un mur tout troué n'est pas impérméable du tout.
		* Ou se dire que l'eau, à l'échelle des insectes, est au minimum sous forme de grosses gouttes.
		* Dans l'idée, une explosion ou un coup ne laisse une couche de pixels abimés ou fragilisés que s'il y a derrière d'autres pixels, eux en meilleurs état.
		* Tandis que des balles vont créer des lignes de pixels abimés, mais mettront du temps à les fragiliser puis à les détruire.
		*/
		std::vector<char> pixels;
		std::vector<int> pixelsState;
		std::vector<sf::Vector2f> avgs;
		
		// methods
		
		void updateGround();
		void updateGround(int x, int y, int dx, int dy);
		
		void damPix(int x, int y, int dmg);
		
	public:
		
		World();
		//~World(/**/); //TODO
		
		std::vector<char>& getPixels();
		std::vector<sf::Vector2f>& getAvgs();
		int getWidth();
		int getHeigh();
		int getNbPix();
		
		void setBackground(std::string texture);
		void setGround(std::string image);
		void loadMap(std::string map, int x, int y);
		//void loadMap(std::string map); //TODO
		
		void damagePix(int x, int y, int dmg);
		void createHole(int x, int y, int radius);
		void damageZone(int x, int y, int dmgMax, int dmgMin, int rad1, int rad2, int rad3);
		
		void draw(sf::RenderWindow & window);
};

#endif
