

//include guard
#ifndef ENTITY_HPP
#define ENTITY_HPP


//included dependencies
#include <string>
#include <stdio.h>				//printf
#include <iostream>				//cin cout
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"	//RenderWindow
#include <iostream>
#include <fstream>
#include <cmath>


class Entity{
	private:
		
		//Frame _frame; //hitbox, graphics
		
		// background
		sf::Texture tex_background;
		sf::Sprite spr_background;
		
		// ground
		sf::Image img_ground;
		sf::Texture tex_ground;
		sf::Sprite terrain;
		
		int width;
		int heigh;
		int nbPix;
		
		std::vector<char> pixels;
		std::vector<sf::Vector2f> avgs;
		
		// methods
		
		void updateGround();
		
	public:
		
		Entity();
		//~Entity(/**/); //TODO
		
		void setBackground(std::string texture);
		void setGround(std::string image);
		void loadMap(std::string map, int x, int y);
		//void loadMap(std::string map); //TODO
		
		void createHole(int x, int y, int radius);
		
		
		void draw(sf::RenderWindow & window);
};

#endif
