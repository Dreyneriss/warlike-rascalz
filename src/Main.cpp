#include "SFML/Graphics.hpp"	//RenderWindow
#include "SFML/Window.hpp"		//Window - VideoMode - Style
#include "SFML/System.hpp"
#include "SFML/Audio.hpp"
#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <time.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <map>
#include <random>

#include "World.hpp"

using namespace std;

float vecToRad(int deltaX, int deltaY){
	
	float cos = deltaX / sqrt(double(deltaX*deltaX + deltaY*deltaY));
	float sin = deltaY / sqrt(double(deltaX*deltaX + deltaY*deltaY));
	
	if(sin > 0){
		return 3.14159/2 + acos(cos);
	}else{
		return 3.14159/2 - acos(cos);
	}
}
float vecToDeg(int deltaX, int deltaY){
	
	float cos = deltaX / sqrt(double(deltaX*deltaX + deltaY*deltaY));
	float sin = deltaY / sqrt(double(deltaX*deltaX + deltaY*deltaY));
	
	if(sin > 0){
		return 90 + acos(cos)*180/3.14159;
	}else{
		return 90 - acos(cos)*180/3.14159;
	}
}
sf::Vector2f radToVec(float rad){
	float cosin = cos(-3.14159/2 + rad);
	float sinus = sin(-3.14159/2 + rad);
	
	return sf::Vector2f(cosin, sinus);
}
sf::Vector2f degToVec(float deg){
	float cosin = cos((deg-90)*3.14159/180);
	float sinus = sin((deg-90)*3.14159/180);
	
	return sf::Vector2f(cosin, sinus);
}

int main(){
  /* initialize random seed: */
	srand (time(NULL));
	
	random_device rd{};
	mt19937 gen{rd()};
	
	normal_distribution<> d{0,-500};
	
	//Boutons (reste à en rajouter)
	//0:haut, 1:bas, 2:gauche, 3:droite,
	//4:clicG, 5:clicD,
	//6:E, 7:F, 8:I, 9:R, 10:A, 11:H
	//12-21:0-9
	bool input[22];
	for(int i=0; i<21; ++i){
		input[i]=false;
	}
	
	//Coordonnées de la souris
	sf::Vector2i sourisInt;
	sf::Vector2f sourisFloat;

	//Création de la fenêtre
	int X = 1200, Y = 800;
	std::string name = "Game";
	sf::RenderWindow window(sf::VideoMode(X, Y), name);
	
	window.setFramerateLimit(120); // limit to 60FPS max (or 120)

	//Lancement du jeu
	sf::Clock globalTime;
	sf::Time elapsedTime;
	sf::Time leftOverTime;
	sf::Time stepTime = sf::milliseconds(16);
	int timeSteps;
	
	// Player
	// liste d'effets : en mouvement, gravité, projeté...
	int ent_radius = 8;
	sf::CircleShape ent_skin(9);
	sf::CircleShape ent_frame(8);
	sf::CircleShape ent_head(6, 3);
	ent_skin.setFillColor(sf::Color::Black);
	ent_frame.setFillColor(sf::Color(100, 250, 50));
	ent_head.setFillColor(sf::Color(200, 50, 0));
	ent_skin.setOrigin(9,9);
	ent_frame.setOrigin(8,8);
	ent_head.setOrigin(6,8);
	ent_head.setRotation(90);
	
	sf::Vector2f ent_speed;
	sf::Vector2f ent_pos0;
	sf::Vector2f ent_pos1;
		
	// World definition
	World world;
	world.setBackground("data/sprites/background.png");
	world.setGround("data/sprites/ground.png");
	world.loadMap("data/sprites/terrain_nohead.txt", 1543, 800);
	
	//Démarrage de la boucle principale
	while (window.isOpen())
	{
		elapsedTime = globalTime.restart();
		
		/*// add time that couldn't be used last frame
		elapsedTime += leftOverTime;
		
		// divide it up in chunks of 16 ms
		timeSteps = elapsedTime.asMilliseconds() / 16;
		
		// store time we couldn't use for the next frame.
		leftOverTime = elapsedTime - stepTime*timeSteps;
		
		for (int i = 0; i < timeSteps; i++) {
			
		}*/
		
		sf::Event event;
		while (window.pollEvent(event))
		{
			//fermeture
			if (event.type == sf::Event::Closed)
				window.close();
			
			//mouvement de la souris
			if (event.type == sf::Event::MouseMoved){
				//jeu.setPosSouris(sf::Mouse::getPosition(window));
				
				sf::Vector2i mouse = sf::Mouse::getPosition(window);
				int deltaX = mouse.x - int(ent_pos0.x);
				int deltaY = mouse.y - int(ent_pos0.y);
				
				float angle = vecToDeg(deltaX, deltaY);
				
				ent_head.setRotation(angle);
			}
				
			
			//clics de la souris
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				input[4] = true;
			else
				input[4] = false;
			
			if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
				input[5] = true;
			else
				input[5] = false;
			
			//touches directionnelles
			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) )
				input[0] = true;
			else
				input[0] = false;
			
			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) )
				input[1] = true;
			else
				input[1] = false;
			
			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) )
				input[2] = true;
			else
				input[2] = false;
			
			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) )
				input[3] = true;
			else
				input[3] = false;
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
				input[6] = true;
			else
				input[6] = false;
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
				input[7] = true;
			else
				input[7] = false;
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::I))
				input[8] = true;
			else
				input[8] = false;
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
				input[9] = true;
			else
				input[9] = false;
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
				input[10] = true;
			else
				input[10] = false;
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
				input[11] = true;
			else
				input[11] = false;
			
			if ( ((sf::Keyboard::isKeyPressed(sf::Keyboard::Num0)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad0))))
				input[12] = true;
			else
				input[12] = false;
			
			if ( ((sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad1)) ) )
				input[13] = true;
			else
				input[13] = false;
			
			if ( ((sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad2)) ) )
				input[14] = true;
			else
				input[14] = false;
			
			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad3)) )
				input[15] = true;
			else
				input[15] = false;
			
			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad4)) )
				input[16] = true;
			else
				input[16] = false;
			
			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::Num5)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad5)) )
				input[17] = true;
			else
				input[17] = false;
			
			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::Num6)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad6)) )
				input[18] = true;
			else
				input[18] = false;

			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::Num7)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad7)) )
				input[19] = true;
			else
				input[19] = false;
			
			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::Num8)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad8)) )
				input[20] = true;
			else
				input[20] = false;
			
			if ( (sf::Keyboard::isKeyPressed(sf::Keyboard::Num9)) or (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad9)) )
				input[21] = true;
			else
				input[21] = false;
		}
		
		if(input[0]){ //Up
			
		}
		if(input[1]){ //Down
			
		}
		if(input[2]){ //Left
			ent_speed.x = -200; //pixel / sec
			
			//ent_skin.move(-1,0);
			//ent_frame.move(-1,0);
			//ent_head.move(-1,0);
		}else if(input[3]){ //Right
			ent_speed.x = 200;
			
			//ent_skin.move(1,0);
			//ent_frame.move(1,0);
			//ent_head.move(1,0);
		}else ent_speed.x = 0;
		if(input[4]){ //left mouse
			bool hit = false;
			bool out = false;
			
			sf::Vector2f dir;
			sf::Vector2f bullet = ent_pos0;
			int pix;
			dir.x = float(sf::Mouse::getPosition(window).x) - ent_pos0.x;
			dir.y = float(sf::Mouse::getPosition(window).y) - ent_pos0.y;
			
			float angle0 = vecToDeg(dir.x, dir.y);
			float angle1 = angle0;
			
			angle1 += float(round(d(gen)))/100;
			
			dir = degToVec(angle1);
			//cout << ent_pos0.x << "," << ent_pos0.y << " ; " << sf::Mouse::getPosition(window).x << "," << sf::Mouse::getPosition(window).y << " ; " << dir.x << "," << dir.y << " ; " << angle0 << " , " << angle1 << endl;
			
			while(!hit and !out){
				bullet += dir;
				pix = int(bullet.x) + int(bullet.y)*world.getWidth();
				if(pix >= 0 and pix < world.getNbPix()){
					if(world.getPixels()[pix]>0)
						hit = true;
				}
				else
					out = true;
			}
			if(hit){
				//world.damagePix(pix%world.getWidth(), pix/world.getWidth(), 200);
				world.damageZone(pix%world.getWidth(), pix/world.getWidth(), 500, 200, 1, 3, 3);
			}
			
		}
		if(input[5]){ //right mouse
			float posX = float(sf::Mouse::getPosition(window).x);
			float posY = float(sf::Mouse::getPosition(window).y);
			
			bool placeable=true;
			for(int i=-ent_radius; i<=ent_radius;++i){
				for(int j=-ent_radius; j<=ent_radius; ++j){
					int k = posX+i + world.getWidth()*(posY+j);
					//cout << k << " : " << i << " , " << j << " ; " << endl;
					if(	((i*i)+(j*j)<=ent_radius*ent_radius) and
						(world.getPixels()[k]>0) ){
						placeable=false;
					}
				}
			}
			
			if(placeable){
				ent_skin.setPosition(posX,posY);
				ent_frame.setPosition(posX,posY);
				ent_head.setPosition(posX,posY);
				ent_pos0.x = posX;
				ent_pos0.y = posY;
				ent_pos1 = ent_pos0;
			}
			
			//cout << elapsedTime.asMicroseconds() << endl;//" , " << leftOverTime.asMilliseconds() << " , " << timeSteps << endl;
			
			//sf::Vector2i mouse = sf::Mouse::getPosition(window);
			//int ind = mouse.x + width*mouse.y;
			//cout << mouse.x << " , " << mouse.y << ", type " << pixels[ind] << ", normale : " << avgs[ind].x << ", " << avgs[ind].y << endl;
		}
		if(input[13]){ //key 1
			sf::Vector2i mouse = sf::Mouse::getPosition(window);
			
			int radius = 32;
			
			world.createHole(mouse.x, mouse.y, radius);
		}
		
		//gestion physique
		
		ent_pos1.x += elapsedTime.asSeconds()*ent_speed.x;
		ent_pos1.y += elapsedTime.asSeconds()*ent_speed.y;
		
		sf::Vector2f ent_posTemp = ent_pos0;
		
		int deltaX = ent_pos1.x - ent_pos0.x;
		int deltaY = ent_pos1.y - ent_pos0.y;
		int steps = deltaX+deltaY;
		bool obstacle = false;
		
		/*for(int s = 0; s < steps; ++s){
			ent_posTemp.x += deltaX/steps;
			ent_posTemp.y += deltaY/steps;
			
			for(int i=-ent_radius; i<=ent_radius;++i){
				for(int j=-ent_radius; j<=ent_radius; ++j){
					int k = ent_posTemp.x+i + world.getWidth()*(ent_posTemp.y+j);
					//cout << k << " : " << i << " , " << j << " ; " << endl;
					if(	((i*i)+(j*j)<=ent_radius*ent_radius) and
						(world.getPixels()[k]>0) ){
						obstacle=true;
						break;
					}
				}
				if(obstacle)
					break;
			}
			if(obstacle){
				ent_posTemp.x -= deltaX/steps;
				ent_posTemp.y -= deltaY/steps;
				break;
			}
		}
		
		if(obstacle){
			ent_pos0 = ent_posTemp;
			ent_skin.setPosition(ent_pos0);
			ent_frame.setPosition(ent_pos0);
			ent_head.setPosition(ent_pos0);
		}else{
			ent_pos0 = ent_pos1;
			ent_skin.setPosition(ent_pos0);
			ent_frame.setPosition(ent_pos0);
			ent_head.setPosition(ent_pos0);
		}*/
		
		ent_skin.setPosition(ent_pos1);
		ent_frame.setPosition(ent_pos1);
		ent_head.setPosition(ent_pos1);
		ent_pos0 = ent_pos1;
		
		sf::Vector2i mouse = sf::Mouse::getPosition(window);
		deltaX = mouse.x - int(ent_pos0.x);
		deltaY = mouse.y - int(ent_pos0.y);
		
		ent_head.setRotation(vecToDeg(deltaX, deltaY));
		
		//affichages

		window.clear();
		
		world.draw(window);
		window.draw(ent_skin);
		window.draw(ent_frame);
		window.draw(ent_head);

		window.display();
		
	}

}
