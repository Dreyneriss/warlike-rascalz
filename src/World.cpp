#include "World.hpp"

World::World(){}
//~World(/**/); //TODO

std::vector<char>& World::getPixels(){
	return pixels;
}
std::vector<sf::Vector2f>& World::getAvgs(){
	return avgs;
}
int World::getWidth(){
	return width;
}
int World::getHeigh(){
	return heigh;
}
int World::getNbPix(){
	return nbPix;
}

void World::updateGround(){
	// premier passage, types 2 (pixels pleins à proximité d'un vide)
	for(int i=0; i<nbPix; ++i){
		if(pixels[i] == 1){
			for(int j=-1; j<=1; ++j){
				for(int k=-1; k<=1; ++k){
					int pixX = (i%width) +j;
					int pixY = (i/width) +k;
					if(	(pixX >= 0 and pixX < width) and
						(pixY >= 0 and pixY < heigh) and
						//(k xor j) and
						(pixels[pixX + (pixY*width)] == 0) ){
						pixels[i] = 2;
						
						sf::Color col = img_ground.getPixel(i%width,i/width);
						col.r /= 2;
						col.g /= 2;
						col.b /= 2;
						
						img_ground.setPixel(i%width,i/width, col);
						
						//img_ground.setPixel(i%width,i/width, sf::Color::Black);
					}
				}
			}
		}
	}
	// second passage, types 3 (pixels pleins à proximité d'un type 2)
	/*for(int i=0; i<nbPix; ++i){
		if(pixels[i] == 1){
			for(int j=-1; j<=1; ++j){
				for(int k=-1; k<=1; ++k){
					int pixX = (i%width) +j;
					int pixY = (i/width) +k;
					if(	(pixX >= 0 and pixX < width) and
						(pixY >= 0 and pixY < heigh) and
						(pixels[pixX + (pixY*width)] == 2) ){
						pixels[i] = 3;
						
						sf::Color col = img_ground.getPixel(i%width,i/width);
						col.r *= 0.75;
						col.g *= 0.75;
						col.b *= 0.75;
						
						img_ground.setPixel(i%width,i/width, col);
					}
				}
			}
		}
	}*/
	tex_ground.loadFromImage(img_ground);
}

void World::updateGround(int x, int y, int dx, int dy){
	// premier passage, types 2 (pixels pleins à proximité d'un vide)
	for(int a = x; a <= x+dx; ++a){
		for(int b = y; b<= y+dy; ++b){
			int i = a + b*width;
			
			if(pixels[i] == 1){
				for(int j=-1; j<=1; ++j){
					for(int k=-1; k<=1; ++k){
						int pixX = (i%width) +j;
						int pixY = (i/width) +k;
						if(	(pixX >= 0 and pixX < width) and
							(pixY >= 0 and pixY < heigh) and
							//(k xor j) and
							(pixels[pixX + (pixY*width)] == 0) ){
							pixels[i] = 2;
							
							sf::Color col = img_ground.getPixel(i%width,i/width);
							col.r /= 2;
							col.g /= 2;
							col.b /= 2;
							
							img_ground.setPixel(i%width,i/width, col);
							
							//img_ground.setPixel(i%width,i/width, sf::Color::Black);
						}
					}
				}
			}
		}
	}
	
	tex_ground.loadFromImage(img_ground);
}

void World::damPix(int x, int y, int dmg){
	int pixState = pixelsState[x + width*y];
	
	pixelsState[x + width*y] -= dmg;
	
	if(pixState>0 and pixelsState[x + width*y]<= 0){ // destroyed
		pixelsState[x + width*y] = 0;
		pixels[x + width*y] = 0;
		
		img_ground.setPixel(x,y, sf::Color::Transparent);
		updateGround(x-2,y-2,5,5);
	} else if(pixState>900 and pixelsState[x + width*y]<= 300){ // from good to weakened
		sf::Color col = img_ground.getPixel(x,y);
		col.r += (255 - col.r)/3;
		col.r += (255 - col.r)/3;
		col.g += (255 - col.g)/3;
		col.b += (255 - col.b)/3;
							
		img_ground.setPixel(x,y, col);
	} else if (pixState>900 and pixelsState[x + width*y]<= 900){ // from good to damaged
		sf::Color col = img_ground.getPixel(x,y);
		col.r += (255 - col.r)/3;
							
		img_ground.setPixel(x,y, col);
	} else if (pixState>300 and pixelsState[x + width*y]<= 300){ // from damaged to weakened
		sf::Color col = img_ground.getPixel(x,y);
		col.r += (255 - col.r)/3;
		col.g += (255 - col.g)/3;
		col.b += (255 - col.b)/3;
							
		img_ground.setPixel(x,y, col);
	}
}

// PUBLIC
		
void World::setBackground(std::string texture){
	tex_background.loadFromFile(texture);
	spr_background.setTexture(tex_background);
}

void World::setGround(std::string image){
	img_ground.loadFromFile(image);
	tex_ground.loadFromImage(img_ground);
	terrain.setTexture(tex_ground);
}

void World::loadMap(std::string map, int X, int Y){
	std::ifstream file(map); //1543 800
	
	width = X;
	heigh = Y;
	nbPix = X*Y;
	
	pixels = std::vector<char>(nbPix);
	pixelsState = std::vector<int>(nbPix);
	
	if(file)
	{
		char c;
		int i = 0;

		while( !file.eof() )
        {
			file.get(c);

			if( c != '\n' ){
				switch(c)
				{
					case '0' :{ //blanc : vide
						pixels[i] = 0;
						pixelsState[i] = 0;
						
						img_ground.setPixel(i%width,i/width, sf::Color::Transparent);
						
						break;
					}
					case '1' :{ //noir : plein
						pixels[i] = 1;
						pixelsState[i] = 1000;
						
						break;
					}
				}
				
				++i;
			}
		}
		
		tex_ground.loadFromImage(img_ground);
		
		file.close();
	}
	else
		std::cout << "Erreur lors du chargement du fichier "<< map << std::endl;//erreur
	
	updateGround();
	
	// calcul des normales
	
	avgs = std::vector<sf::Vector2f>(nbPix);
	
	for(int i=0; i<nbPix; ++i){
		for(int x=-3; x<=3; ++x){
			for(int y=-3; y<=3; ++y){
				if(pixels[i+x + (y*width)]>0){
					avgs[i].x -= x;
					avgs[i].y -= y;
				}
			}
		}
		float length = std::sqrt((avgs[i].x*avgs[i].x) + (avgs[i].y*avgs[i].y)); // distance from avg to the center
		avgs[i].x /= length;
		avgs[i].y /= length;
    }
}


void World::damagePix(int x, int y, int dmg){
	damPix(x,y,dmg);
	
	tex_ground.loadFromImage(img_ground);
}


void World::damageZone(int x, int y, int dmgMax, int dmgMin, int rad1, int rad2, int rad3){
	for(int i = -rad3-1 ; i <= rad3+1 ; ++i){
		for(int j = -rad3-1 ; j <= rad3+1 ; ++j){
			int pixX = x + i;
			int pixY = y + j;
			int pix = pixX + width*pixY;
			
			if( (i*i)+(j*j)<=(rad1*rad1) ){ //coeur de l'explosion
				damPix(pixX, pixY, dmgMax);
			} else if ( (i*i)+(j*j)<=(rad2*rad2) ){ //couronne de l'explosion
				double dist = std::sqrt( ((i)*(i))+((j)*(j)) ); // distance de rad1
				double ratio = (dist - rad1)/(rad2 - rad1);
				double dmg = dmgMax - (dmgMax - dmgMin)*ratio;
				
				damPix(pixX, pixY, int(dmg));
			} else if ( (i*i)+(j*j)<=(rad3*rad3) ){ //souffle de l'explosion
				damPix(pixX, pixY, dmgMin);
			} //else, hors de l'explosion
		}
	}
	
	tex_ground.loadFromImage(img_ground);
}

void World::createHole(int targetX, int targetY, int radius){
	for(int x=-radius; x<=radius; ++x){
		for(int y=-radius; y<=radius; ++y){
			if( ((x*x)+(y*y) <= 12*12) and
			(targetX+x >= 0) and (targetY+y >= 0) )
			//and ne dépasse pas la fin de l'img
			{
				img_ground.setPixel(targetX+x, targetY+y, sf::Color::Transparent);
				pixels[targetX+x + width*(targetY+y)]=0;
			}
		}
	}
	
	updateGround(targetX - radius - 2, targetY - radius - 2, (2*radius)+4, (2*radius)+4);
}

void World::draw(sf::RenderWindow & window){
	window.draw(spr_background);
	window.draw(terrain);
}
