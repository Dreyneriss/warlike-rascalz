Titres alternatifs:
Warlike Rascalz (sonne bien, commence presque comme Worms)
Belli Rascalz (Belli de bellicose, de plus il se pourrait qu'ils soient plutôt ventrus)

Note sur la maniabilité :
Worms se jouait essentiellement au clavier, pour viser comme pour tirer. La puissance du tir dépendait du temps que l'on appuyait sur la barre espace.
Ici, je verrais plutôt une maniabilité utilisant d'avantage la souris.
